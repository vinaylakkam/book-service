package com.vspace.bookservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RequestMapping("/books")
@RestController
public class BookResource {

  @Autowired
  BookService bookService;

  @GetMapping
  List<Book>  findAll() {
    return bookService.findAll();
  }

  @GetMapping("/{bookId}")
  Book  findOne(@PathVariable Long bookId) {
    return bookService.findById(bookId);
  }

  @PostMapping
  ResponseEntity<Book> create(@RequestBody Book book) {

    Book savedBook = bookService.create(book);

    final URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(savedBook.getId())
            .toUri();

    return ResponseEntity.created(uri)
            .body(savedBook);
  }
}
