package com.vspace.bookservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {

  @Autowired
  BookRepository bookRepository;

  public List<Book> findAll() {
    return bookRepository.findAll();
  }

  public Book findById(Long bookId) {
    return bookRepository.findById(bookId)
            .orElseThrow(()->new RuntimeException("Book not found: " + bookId));
  }

  public Book create(Book book) {
    return bookRepository.save(book);
  }
}
